#include "deletedialog.h"

#include <QDialogButtonBox>
#include <QDir>
#include <QFileInfo>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

DeleteDialog::DeleteDialog(const QList<QString> deletion_list, QWidget *parent)
    : QDialog(parent)
{
    m_deletion_list = deletion_list;

    m_button_box = new QDialogButtonBox(QDialogButtonBox::Yes | QDialogButtonBox::Cancel);
    m_list_widget = new QListWidget(this);

    setup_ui();

    connect(m_button_box, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(m_button_box, &QDialogButtonBox::accepted, this, &DeleteDialog::execute_deletion);
}

void DeleteDialog::setup_ui()
{
    setWindowTitle("Delete items");
    setModal(true);
    resize(400, 100);

    //layout
    QVBoxLayout* vertical_layout = new QVBoxLayout(this);
    setLayout(vertical_layout);

    vertical_layout->addWidget(new QLabel("Are you sure you want to delete the following items?"));
    vertical_layout->addWidget(m_button_box);
    vertical_layout->addWidget(m_list_widget);

    foreach(QString path, m_deletion_list) {
        m_list_widget->addItem(path);
    }
}

void DeleteDialog::execute_deletion()
{
    foreach(QString path, m_deletion_list) {
        QFileInfo fi(path);
        if (fi.isFile()) {
            QFile file(path);
            file.remove();
            //qDebug() << "delete file" << fi;
        }
        else if (fi.isDir())  {
            QDir dir(fi.absoluteFilePath());

            dir.removeRecursively();
            //qDebug() << "delete dir" << dir;
        }
    }

    accept();
}
