#ifndef CREATEDIRECTORYDIALOG_H
#define CREATEDIRECTORYDIALOG_H

#include "../models/filesystemmodel.h"

#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>

class CreateDirectoryDialog : public QDialog
{
public:
    CreateDirectoryDialog(const FileSystemModel *model, QWidget *parent = 0);

private:
    const FileSystemModel* m_model;

    QLineEdit* m_line_edit;
    QDialogButtonBox* m_button_box;

    void setup_ui();

private slots:
    void on_accepted();
};

#endif // CREATEDIRECTORYDIALOG_H
