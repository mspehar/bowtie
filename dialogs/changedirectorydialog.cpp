#include "changedirectorydialog.h"

#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QVBoxLayout>

ChangeDirectoryDialog::ChangeDirectoryDialog(FileSystemModel *model, QWidget *parent)
    : QDialog(parent)
    , m_model(model)
{
    m_line_edit = new QLineEdit(this);
    m_button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    setup_ui();
}

void ChangeDirectoryDialog::setup_ui()
{

}
