#include "createdirectorydialog.h"

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QVBoxLayout>

CreateDirectoryDialog::CreateDirectoryDialog(const FileSystemModel *model, QWidget *parent )
    : QDialog(parent)
    , m_model(model)
{
    m_line_edit = new QLineEdit(this);
    m_button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    setup_ui();
}

void CreateDirectoryDialog::setup_ui()
{

    setWindowTitle("Create Directory");
    setModal(true);

    resize(400, 100);

    //layout
    QVBoxLayout* vertical_layout = new QVBoxLayout(this);
    setLayout(vertical_layout);

    vertical_layout->addWidget(new QLabel("Name", this));
    vertical_layout->addWidget(m_line_edit);

    vertical_layout->addWidget(new QLabel("Location: " + m_model->path()));

    vertical_layout->addStretch(100);

    vertical_layout->addWidget(m_button_box);

    connect(m_button_box, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_button_box, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(m_button_box, &QDialogButtonBox::accepted, this, &CreateDirectoryDialog::on_accepted);
}

void CreateDirectoryDialog::on_accepted()
{
    m_model->create_directory(m_line_edit->text());
}
