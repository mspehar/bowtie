#ifndef CHANGEDIRECTORYDIALOG_H
#define CHANGEDIRECTORYDIALOG_H

#include "../models/filesystemmodel.h"

#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>

class ChangeDirectoryDialog : public QDialog
{
public:
    ChangeDirectoryDialog(FileSystemModel *model, QWidget *parent = 0);

private:
    FileSystemModel *m_model;

    QLineEdit* m_line_edit;
    QDialogButtonBox* m_button_box;

    void setup_ui();
};

#endif // CHANGEDIRECTORYDIALOG_H
