#ifndef DELETEDIALOG_H
#define DELETEDIALOG_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QListWidget>

class DeleteDialog : public QDialog
{
public:
    DeleteDialog(const QList<QString> deletion_list, QWidget *parent = 0);

private:
    QList<QString> m_deletion_list;

    QDialogButtonBox* m_button_box;
    QListWidget* m_list_widget;

    void setup_ui();

private slots:
    void execute_deletion();
};

#endif // DELETEDIALOG_H
