#ifndef MOVECOPYDIALOG_H
#define MOVECOPYDIALOG_H

#include "../models/enums.h"
#include "../models/modelmanager.h"
#include "../widgets/progressbar.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QProgressBar>
#include <QWidget>

class MoveCopyDialog : public QDialog
{
    Q_OBJECT

public:
    MoveCopyDialog(ModelManager *manager, QList<QString> source_list, QString source_dir, QString destination_dir, FileSystemOperationType m_operation_type, QWidget *parent = 0);

private:
    ModelManager *m_manager;
    QList<QString> m_source_list;
    QString m_source_dir;
    QString m_destination_dir;
    FileSystemOperationType m_operation_type;

    QDialogButtonBox* m_button_box;
    QLineEdit* m_line_edit_from;
    QLineEdit* m_line_edit_to;
    QProgressBar* m_progress_bar_total_count;
    QProgressBar* m_progress_bar_total_bytes;
    QProgressBar* m_progress_bar_file_bytes;
    QLabel *m_current_file_label;

    qreal m_progress_total_count = 0;
    qreal m_progress_total_bytes = 0;
    qreal m_progress_file_bytes = 0;
    FileSystemOperation *m_fsop;

    void setup_ui();

    void ui_show_progress();
    void ui_hide_progress();

private slots:
    void disable_ok_button(QString text);
    void report_progress_total(qreal count_progress, qreal bytes_progress);
    void report_progress_file_bytes(QString file, qreal progress);
    void ok();
    void cancel();

signals:
    void submitted();
};

#endif // MOVECOPYDIALOG_H
