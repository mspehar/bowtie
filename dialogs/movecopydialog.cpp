#include "movecopydialog.h"

#include "../models/filesystemoperation.h"
#include "../widgets/progressbar.h"

#include <QDir>
#include <QLabel>
#include <QListWidgetItem>
#include <QPushButton>
#include <QProgressBar>
#include <QVBoxLayout>

MoveCopyDialog::MoveCopyDialog(ModelManager *manager, QList<QString> source_list, QString source_dir, QString destination_dir, FileSystemOperationType operation_type, QWidget *parent)
    : QDialog(parent)
    , m_manager(manager)
    , m_source_list(source_list)
    , m_source_dir(source_dir)
    , m_destination_dir(destination_dir)
    , m_operation_type(operation_type)
{
    m_line_edit_from = new QLineEdit(this);
    m_line_edit_to = new QLineEdit(this);
    m_button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    m_progress_bar_total_count = new QProgressBar(this);
    m_progress_bar_total_bytes = new QProgressBar(this);
    m_progress_bar_file_bytes = new QProgressBar(this);
    m_current_file_label = new QLabel(this);

    connect(m_button_box, &QDialogButtonBox::rejected, this, &MoveCopyDialog::cancel);
    connect(m_button_box, &QDialogButtonBox::accepted, this, &MoveCopyDialog::ok);
    connect(m_line_edit_to, &QLineEdit::textEdited, this, &MoveCopyDialog::disable_ok_button);

    setup_ui();

    m_fsop = new FileSystemOperation(m_source_list, m_destination_dir, m_operation_type);

    connect(m_fsop, &FileSystemOperation::report_progress_total, this, &MoveCopyDialog::report_progress_total);
    connect(m_fsop, &FileSystemOperation::report_progress_file_bytes, this, &MoveCopyDialog::report_progress_file_bytes);
    connect(m_fsop, &FileSystemOperation::completed, this, &QDialog::accept);
    connect(m_fsop, &FileSystemOperation::cancelled, this, &QDialog::reject);
}

void MoveCopyDialog::setup_ui()
{
    if (m_operation_type == FileSystemOperationType::COPY) {
        setWindowTitle("Copy");
    } else if (m_operation_type == FileSystemOperationType::MOVE) {
        setWindowTitle("Move");
    }

    setModal(true);

    m_line_edit_from->setEnabled(false);
    m_line_edit_to->setEnabled(true);

    resize(600, 200);

    m_line_edit_from->setText(m_source_dir);
    m_line_edit_to->setText(m_destination_dir);

    //layout
    QVBoxLayout* vertical_layout = new QVBoxLayout(this);
    setLayout(vertical_layout);

    vertical_layout->addWidget(new QLabel("From", this));
    vertical_layout->addWidget(m_line_edit_from);

    vertical_layout->addWidget(new QLabel("To", this));
    vertical_layout->addWidget(m_line_edit_to);

    //vertical_layout->addStretch(100);

    vertical_layout->addWidget(m_button_box);

    QFrame* ruler = new QFrame(this);
    ruler->setFrameShadow(QFrame::Sunken);
    ruler->setFrameShape(QFrame::HLine);
    vertical_layout->addWidget(ruler);

    //vertical_layout->addWidget(m_list_widget);
    vertical_layout->addWidget(new QLabel("Total fines copied progress", this));
    vertical_layout->addWidget(m_progress_bar_total_count);
    m_progress_bar_total_count->setAlignment(Qt::AlignHCenter);

    vertical_layout->addWidget(new QLabel("Bytes copied progress", this));
    vertical_layout->addWidget(m_progress_bar_total_bytes);
    m_progress_bar_total_bytes->setAlignment(Qt::AlignHCenter);

    vertical_layout->addWidget(new QLabel("File copy progress", this));
    vertical_layout->addWidget(m_progress_bar_file_bytes);
    m_progress_bar_file_bytes->setAlignment(Qt::AlignHCenter);

    vertical_layout->addWidget(m_current_file_label);

    /*
    int i = 0;

    foreach (QString source_path, m_source_list) {
        QListWidgetItem *item = new QListWidgetItem(m_list_widget);
        ProgressBar *pb = new ProgressBar(source_path, m_list_widget);

        pb->setValue((i+3) * 10);
        pb->setStyleSheet("QProgressBar { border: 0px solid grey; border-radius: 0px; text-align: left; } QProgressBar::chunk { background-color: #3add36; width: 1px; }");

        m_list_widget->insertItem(i, item);
        m_list_widget->setItemWidget(item, pb);

        i++;
    }
    */

    //connect(m_button_box, &QDialogButtonBox::accepted, this, &QDialog::accept);

    //connect(this, &MoveCopyDialog::submitted, this, &QDialog::accept);
    //connect(m_fsop, &FileSystemOperation::done, this, &QDialog::accept);

    disable_ok_button("");
}

void MoveCopyDialog::ui_show_progress()
{
    m_line_edit_to->setEnabled(false);
}

void MoveCopyDialog::ui_hide_progress()
{
    m_line_edit_to->setEnabled(true);
}

void MoveCopyDialog::disable_ok_button(QString text)
{
    if (QDir(m_source_dir) == QDir(m_line_edit_to->text())) {
        m_button_box->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void MoveCopyDialog::ok()
{
    m_line_edit_to->setEnabled(false);
    m_button_box->button(QDialogButtonBox::Ok)->setEnabled(false);

    m_manager->m_fsop_manager.add_fsop_to_queue(m_fsop);

    emit submitted();
    m_fsop->start();
}

void MoveCopyDialog::cancel()
{
    m_fsop->cancel();
}

void MoveCopyDialog::report_progress_total(qreal count_progress, qreal bytes_progress)
{
    m_progress_total_count = count_progress;
    m_progress_bar_total_count->setValue(m_progress_total_count);

    m_progress_total_bytes = bytes_progress;
    m_progress_bar_total_bytes->setValue(m_progress_total_bytes);
}

void MoveCopyDialog::report_progress_file_bytes(QString file, qreal progress)
{
    m_progress_file_bytes = progress;
    m_progress_bar_file_bytes->setValue(m_progress_file_bytes);
    m_current_file_label->setText(file);
}
