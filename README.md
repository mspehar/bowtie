# What is Bowtie

Midnight Commander clone in Qt

# Status

Just beginning, **not recommended to be used at this point of time**

If you do decide to try it out, you will encounter the following:
- Error handling not implemented, if something is not possible it will just skip it and move on
- No settings, currently only convention is considered
  - save tabs at exit
  - shortcuts as in midnight commander
  - some basic features in windows explorer are not yet implemented (e.g. F2 for rename)
- Some shortcut handling is not yet properly working
- Select all / deselect all does not work
- ...

# Screenshot

[![Screenshot](https://i.imgur.com/PmE9ubY.png)](https://i.imgur.com/PmE9ubY.png)
