#include "settingsmodel.h"

#include "enums.h"

#include <QMap>
#include <QSettings>

SettingsModel::SettingsModel()
{

}


void SettingsModel::save_tabs(QMap<WidgetSide, QList<QString>> paths)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "bowtie", "bowtie");

    settings.beginGroup("left");
    settings.setValue("Tabs", paths[WidgetSide::LEFT]);
    settings.endGroup();

    settings.beginGroup("right");
    settings.setValue("Tabs", paths[WidgetSide::RIGHT]);
    settings.endGroup();
}

QMap<WidgetSide, QList<QString>> SettingsModel::read_tabs()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "bowtie", "bowtie");

    settings.beginGroup("left");
    QList<QString> _left = settings.value("Tabs").toStringList();
    settings.endGroup();

    settings.beginGroup("right");
    QList<QString> _right = settings.value("Tabs").toStringList();
    settings.endGroup();

    QMap<WidgetSide, QList<QString>> _map;

    _map[WidgetSide::LEFT] = _left;
    _map[WidgetSide::RIGHT] = _right;

    return _map;
}
