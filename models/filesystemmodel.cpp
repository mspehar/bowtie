#include "filesystemmodel.h"

#include "enums.h"

#include <QMessageBox>
#include <QAbstractItemModel>
#include <QDir>
#include <QStringList>
#include <QFileIconProvider>
#include <QStorageInfo>
#include <QVariant>

FileSystemModel::FileSystemModel(QObject *parent) : QAbstractItemModel(parent)
{
    m_columns[0] = "Name";
    m_columns[1] = "Ext";
    m_columns[2] = "Size";
    m_columns[3] = "Date";
    //m_columns[4] = "Attr";

    connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, &FileSystemModel::populate);
}


QModelIndex FileSystemModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column);
}

QModelIndex FileSystemModel::parent(const QModelIndex &index) const
{
    return QModelIndex();
}

int FileSystemModel::rowCount(const QModelIndex& parent) const
{
    return m_data.size();
}
int FileSystemModel::columnCount(const QModelIndex& parent) const
{
    return m_columns.size();
}

QVariant FileSystemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    QFileInfo fi = m_data[index.row()];

    if (role == Qt::DisplayRole) {
        if (index.column() == 0) {
            return fi.fileName();
        }
        else if (index.column() == 1) {
            if (!fi.isDir()) {
                return fi.suffix();
            }
        }
        else if (index.column() == 2) {
            if (fi.isDir()) {
                return "<DIR>";
            } else {
                return formatted_byte_size(fi.size());
            }
        }
        else if (index.column() == 3) {
            return fi.lastModified().toString("yyyy-MM-dd hh:mm:ss");
        }
        else if (index.column() == 4) {
            return "permissions";
        }
    }

    if (role == DataRoles::ABSOLUTE_PATH) {
        return fi.filePath();
    }

    if (role == DataRoles::SELECTED) {
        return m_marked.contains(fi);
    }

    if (role == Qt::DecorationRole ) {
        if (index.column() == 0) {
            return m_icon_provider.icon(fi);
        }
    }

    return QVariant();
}

QVariant FileSystemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole) {
            return QVariant(m_columns[section]);
        }
        else {
            return QVariant();
        }
    }
    else return QVariant();
}

bool FileSystemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    return false;
}

/**************************/

void FileSystemModel::populate()
{
    beginResetModel();
    m_data.clear();
    m_marked.clear();

    QList<QFileInfo> file_list = QDir(m_path).entryInfoList(QDir::NoFilter, QDir::DirsFirst);

    // add ".." dir if we are not at the root
    QFileInfo _path(m_path);
    if (!_path.isRoot()) {
        m_data.append(QFileInfo(_path.absoluteFilePath(), ".."));
    }

    foreach (QFileInfo fi, file_list) {
        if (fi.fileName() != "." and fi.fileName() != "..") {
            m_data.append(fi);
        }
    }

    endResetModel();
}

void FileSystemModel::set_path(QString path)
{
    if (path.length() > 0) {
        QString old_path = m_path;

        m_watcher.removePath(m_path);
        m_path = path;
        m_watcher.addPath(m_path);

        populate();

        emit path_changed(old_path, m_path);
    }
}

QString FileSystemModel::path() const
{
    return m_path;
}

QFileInfo FileSystemModel::file_info(const QModelIndex &index)
{
    return m_data[index.row()];
}

QList<QString> FileSystemModel::marked_list() const
{
    QList<QString> marked_list;

    if (m_marked.length() > 0) {
        foreach(QFileInfo fi, m_marked) {
            marked_list.append(fi.filePath());
        }
    } else {

    }

    return marked_list;
}

QString FileSystemModel::formatted_byte_size(qint64 size) const
{
    double kb = size / 1024;
    double mb = kb / 1024;
    double gb = mb / 1024;

    if (int(gb) > 0) {
        return QString().asprintf("%2.2fG", gb);
    } else if (int(mb) > 0) {
        return QString().asprintf("%2.2fM", mb);
    } else if (int(kb) > 0) {
        return QString().asprintf("%2.2fK", kb);
    } else {
        return QString::number(size);
    }

    return "Error formatting";
}

void FileSystemModel::flip_marked(const QModelIndex &index)
{
    QFileInfo fi = m_data[index.row()];

    if (m_marked.contains(fi)) {
        m_marked.removeOne(fi);
    } else {
        m_marked.append(fi);
    }

    QList<int> roles;
    roles.append(DataRoles::SELECTED);

    emit dataChanged(index.siblingAtColumn(0), index.siblingAtColumn(index.model()->columnCount()-1), roles);
}

bool FileSystemModel::create_directory(QString name) const
{
    QDir dir(path());

    return dir.mkdir(name);
}

qreal FileSystemModel::storage_usage()
{
    QStorageInfo storage(m_path);

    qreal ratio = (qreal)storage.bytesAvailable() / (qreal)storage.bytesTotal();
    qreal percentage = (1-ratio) * 100;

    return percentage;
}

int FileSystemModel::marked_count() const
{
    return m_marked.length();
}
