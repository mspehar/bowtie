#include "filesystemoperation.h"

#include "enums.h"
#include "filesystemoperationworker.h"

#include <QDebug>

FileSystemOperation::FileSystemOperation(QList<QString> source_list, QString destination_dir, FileSystemOperationType operation_type)
    : QObject()
    , m_source_list(source_list)
    , m_destination_dir(destination_dir)
    , m_operation_type(operation_type)
{
    m_worker = new FileSystemOperationWorker(m_source_list, m_destination_dir, m_operation_type);

    /* worked closes itself */
    connect(m_worker, &FileSystemOperationWorker::finished, m_worker, &FileSystemOperationWorker::deleteLater);

    /* worker tells parent results */
    connect(m_worker, &FileSystemOperationWorker::report_progress_total, this, &FileSystemOperation::report_progress_total);
    connect(m_worker, &FileSystemOperationWorker::report_progress_file_bytes, this, &FileSystemOperation::report_progress_file_bytes);
    connect(m_worker, &FileSystemOperationWorker::completed, this, &FileSystemOperation::completed);
    connect(m_worker, &FileSystemOperationWorker::cancelled, this, &FileSystemOperation::cancelled);
}

void FileSystemOperation::start()
{
    m_worker->start();
}

void FileSystemOperation::cancel()
{
    m_worker->cancel();

    m_worker->terminate();
    m_worker->wait();
}

void FileSystemOperation::pause()
{

}

