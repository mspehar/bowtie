#include "filesystemoperationworker.h"

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QStorageInfo>
#include <QThread>

FileSystemOperationWorker::FileSystemOperationWorker(QList<QString> source_list, QString destination_dir, FileSystemOperationType operation_type)
    : QThread()
    , m_source_list(source_list)
    , m_destination_dir(destination_dir)
    , m_operation_type(operation_type)
{

}

void FileSystemOperationWorker::run()
{
    if (m_operation_type == FileSystemOperationType::COPY) {
        collect_info();
        create_destination_dirs();
        copy_files();
    } else if (m_operation_type == FileSystemOperationType::MOVE) {
        QStorageInfo src_si(m_source_list[0]);
        QStorageInfo dst_si(m_destination_dir);

        if (src_si.name() == dst_si.name()) {
            //rename
            qDebug() << src_si << src_si.name();
            qDebug() << dst_si << dst_si.name();
            rename();
        } else {
            // copy, then delete
            collect_info();
            create_destination_dirs();
            copy_then_delete();
        }
    }

    emit completed();
}

bool FileSystemOperationWorker::collect_info()
{
    for (int i=0; i< m_source_list.length(); i++) {
        QFileInfo source_fi(m_source_list[i]);
        QFileInfo destination_fi(m_destination_dir);

        if (source_fi.isFile() && destination_fi.isDir()) {
            QString relative_source = source_fi.dir().relativeFilePath(source_fi.absoluteFilePath());
            QString addon = "";
            if (destination_fi.absoluteFilePath().right(1) != "/") {
                addon = "/";
            }
            QString absolute_destination = destination_fi.absoluteFilePath() + addon + relative_source;

            m_list_of_file_transfers[source_fi.absoluteFilePath()] = absolute_destination;
            m_bytes += source_fi.size();
        } else if (source_fi.isDir() && destination_fi.isDir()) {
            QDirIterator iterator(source_fi.absoluteFilePath(), QDirIterator::Subdirectories);

            while (iterator.hasNext()) {
                if (m_cancel) {
                    return false;
                }

                QFileInfo entry_fi = iterator.nextFileInfo();
                QString relative_source = source_fi.dir().relativeFilePath(entry_fi.absoluteFilePath());
                QString addon = "";
                if (destination_fi.absoluteFilePath().right(1) != "/") {
                    addon = "/";
                }
                QString absolute_destination = destination_fi.absoluteFilePath() + addon + relative_source;

                if (entry_fi.isFile()) {
                    m_list_of_file_transfers[entry_fi.absoluteFilePath()] = absolute_destination;
                    m_bytes += entry_fi.size();
                }
                else if (entry_fi.isDir()) {
                    if (relative_source != ".") {
                        m_list_of_dirs.insert(absolute_destination);
                    }
                }
            }
        }

        float progress = ((float) i / float(m_source_list.length())) * 100;
        emit resultReady(source_fi.absoluteFilePath(), progress);
    }

    return true;
}


void FileSystemOperationWorker::cancel()
{
    m_cancel = true;

    emit cancelled();
}


bool FileSystemOperationWorker::create_destination_dirs()
{
    QFileInfo destination_fi(m_destination_dir);

    foreach (QString d, m_list_of_dirs) {
        if (m_cancel) {
            return false;
        }

        QDir dir(destination_fi.dir());

        dir.mkpath(d);
    }

    return true;
}

bool FileSystemOperationWorker::copy_files()
{
    QList<QString> keys = m_list_of_file_transfers.keys();

    unsigned long long int bytes_transferred = 0;

    qint64 buffer_size = 4194304;
    QByteArray buffer;

    for (int i=0; i<keys.length(); i++) {
        if (m_cancel) {
            return false;
        }

        QString path_src = keys[i];
        QString path_dst = m_list_of_file_transfers[path_src];

        QFile f_src(path_src);
        QFile f_dst(path_dst);

        bytes_transferred += 0;

        //QFile::copy(f_src, f_dst);
        if (f_src.open(QIODevice::ReadOnly) && f_dst.open(QIODevice::WriteOnly)) {
            qint64 internal_buffer_size = f_src.size() < buffer_size ? f_src.size() : buffer_size;

            while (1) {
                buffer = f_src.read(internal_buffer_size);
                if (buffer.isEmpty()) {
                    break;
                }

                f_dst.write(buffer);

                bytes_transferred += internal_buffer_size;

                qreal pct_file_bytes = (qreal)(bytes_transferred) / f_src.size() * 100;

                emit report_progress_file_bytes(path_src, pct_file_bytes);
            }

            f_src.close();
            f_dst.close();
        }

        /*
        unsigned long long int single_chunk = src_fi.size()/4;

        for (int j=0; j<4; j++) {
            QThread::msleep(100);
            bytes_transferred += single_chunk;

            qreal pct_file_bytes = (qreal)(single_chunk * (j+1)) / src_fi.size() * 100;

            emit report_progress_file_bytes(f_src, pct_file_bytes);
        }
        */

        emit report_progress_file_bytes(path_src, 100.0);

        qreal pct_total_count = ((i+1) / (float)keys.length()) * 100;
        qreal pct_total_bytes = std::trunc((float)((qreal)bytes_transferred / m_bytes) * 100);
        emit report_progress_total(pct_total_count, pct_total_bytes);
    }

    emit report_progress_total(100.0, 100.0);

    return true;
}

bool FileSystemOperationWorker::copy_then_delete()
{
    copy_files();

    foreach (QString f_src, m_list_of_file_transfers.keys()) {
        if (m_cancel) {
            return false;
        }
    }

    return true;
}

bool FileSystemOperationWorker::rename()
{
    foreach (QString f_src, m_source_list) {
        if (m_cancel) {
            return false;
        }

        QFileInfo src_fi(f_src);
        QFileInfo dst_fi(m_destination_dir);

        QString addon = "";
        if (dst_fi.absoluteFilePath().right(1) != "/") {
            addon = "/";
        }

        QString f_dst = m_destination_dir + addon + src_fi.fileName();
    }

    return true;
}
