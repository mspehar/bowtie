#ifndef FILESYSTEMMODEL_H
#define FILESYSTEMMODEL_H

#include <QFileSystemWatcher>
#include <QFileIconProvider>
#include <QAbstractItemModel>
#include <QDir>
#include <QFileInfo>

class FileSystemModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    FileSystemModel(QObject *parent = nullptr);

    /* reimplement methods */
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    /* custom methods */
    void set_path(QString path);
    void flip_marked(const QModelIndex &index);
    QString path() const;
    QList<QString> marked_list() const;
    int marked_count() const;
    QFileInfo file_info(const QModelIndex &index);
    qreal storage_usage();
    bool create_directory(QString name) const;

public slots:
    void populate();

private:
    QString m_path;
    QFileIconProvider m_icon_provider;
    QFileSystemWatcher m_watcher;

    QMap<int, QString> m_columns;
    QList<QFileInfo> m_data;
    QList<QFileInfo> m_marked;

    QString formatted_byte_size(qint64 size) const;

signals:
    void path_changed(QString old_path, QString path);
};

#endif // FILESYSTEMMODEL_H
