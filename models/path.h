#ifndef PATH_H
#define PATH_H

/*
    inspired by Python's pathlib.Path
*/

#include <QDateTime>
#include <QFileInfo>
#include <QList>
#include <QString>


class Path
{
public:
    Path(QFileInfo path);
    Path(QString path);

    virtual ~Path(){};
    virtual QList<QString> pathlib_parts();

    virtual QString pathlib_drive();
    virtual QString pathlib_drive_root_path();
    virtual QString pathlib_root();
    virtual QString pathlib_anchor();
    virtual QString pathlib_name();
    virtual QString pathlib_suffix();
    virtual QList<QString> pathlib_suffixes();
    virtual QString pathlib_stem();

    virtual QString pathlib_as_uri();
    virtual QString pathlib_as_posix();

    /*
    bool is_absolute();
    bool is_relative_to(PurePath other);

    PurePath joinpath(PurePath other);
    PurePath joinpath(QString other);

    bool match(QString pattern);

    PurePath relative_to(PurePath other);
    PurePath relative_to(QString other);

    PurePath with_name(QString name);
    PurePath with_stem(QString stem);
    PurePath with_suffix(QString stem);
    */

    virtual QString pathlib_to_string();
    virtual QDateTime pathlib_last_modified();
    virtual qint64 pathlib_size();
    virtual QString pathlib_permissionsAsString();
};

#endif // PATH_H
