#ifndef ENUMS_H
#define ENUMS_H

enum DataRoles {
    ABSOLUTE_PATH = 10001,
    SELECTED = 10002,
    SORT_ORDER = 10003
};

enum FileSystemOperationType {
    COPY,
    MOVE,
    DELETE,
    UNKNOWN
};

enum WidgetSide {
    LEFT,
    RIGHT
};

#endif // ENUMS_H
