#ifndef FILESYSTEMOPERATIONWORKER_H
#define FILESYSTEMOPERATIONWORKER_H

#include "enums.h"

#include <QMap>
#include <QThread>
#include <QObject>
#include <QSet>

class FileSystemOperationWorker : public QThread
{
    Q_OBJECT
public:
    FileSystemOperationWorker(QList<QString> source_list, QString destination_dir, FileSystemOperationType operation_type);

    void run() override;

private:
    QList<QString> m_source_list;
    QString m_destination_dir;
    FileSystemOperationType m_operation_type;
    bool m_cancel = false;

    QSet<QString> m_list_of_dirs;
    QMap<QString, QString> m_list_of_file_transfers;
    unsigned long long int m_bytes = 0;

    bool collect_info();
    bool create_destination_dirs();
    bool copy_files();
    bool copy_then_delete();
    bool rename();

public slots:
    void cancel();

signals:
    void resultReady(const QString &s, const float progress);

    void report_progress_total(qreal count_progress, qreal bytes_progress);
    void report_progress_file_bytes(QString file, qreal progress);

    void completed();
    void cancelled();
};

#endif // FILESYSTEMOPERATIONWORKER_H
