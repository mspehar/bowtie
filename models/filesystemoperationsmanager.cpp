#include "filesystemoperationsmanager.h"

FileSystemOperationsManager::FileSystemOperationsManager()
{

}

void FileSystemOperationsManager::add_fsop_to_queue(FileSystemOperation *fsop)
{
    FileSystemOperation *_fsop = fsop;
    m_fsop_queue.append(_fsop);
    _fsop->start();
}
