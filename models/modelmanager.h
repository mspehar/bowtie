#ifndef MODELMANAGER_H
#define MODELMANAGER_H

#include "filesystemoperationsmanager.h"
#include "settingsmodel.h"

#include <QObject>

class ModelManager : public QObject
{
    Q_OBJECT

public:
    ModelManager();
    ~ModelManager();

public:
    SettingsModel m_settings_model;
    FileSystemOperationsManager m_fsop_manager;
};

#endif // MODELMANAGER_H
