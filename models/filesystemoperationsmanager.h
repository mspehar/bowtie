#ifndef FILESYSTEMOPERATIONSMANAGER_H
#define FILESYSTEMOPERATIONSMANAGER_H

#include "filesystemoperation.h"

class FileSystemOperationsManager
{
public:
    FileSystemOperationsManager();

    void add_fsop_to_queue(FileSystemOperation *fsop);

private:
    QList<FileSystemOperation*> m_fsop_queue;
};

#endif // FILESYSTEMOPERATIONSMANAGER_H
