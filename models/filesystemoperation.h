#ifndef FILESYSTEMOPERATION_H
#define FILESYSTEMOPERATION_H

#include "enums.h"
#include "filesystemoperationworker.h"

#include <QObject>

class FileSystemOperation : public QObject
{
    Q_OBJECT

public:
    FileSystemOperation(QList<QString> source_list, QString destination_dir, FileSystemOperationType operation_type);

    void start();
    void cancel();
    void pause();
    void request_cancelation();

private:
    QList<QString> m_source_list;
    QString m_destination_dir;
    FileSystemOperationType m_operation_type;
    FileSystemOperationWorker* m_worker;

signals:
    void report_progress_total(qreal count_progress, qreal bytes_progress);
    void report_progress_file_bytes(QString file, qreal progress);
    void completed();
    void cancelled();
};

#endif // FILESYSTEMOPERATION_H
