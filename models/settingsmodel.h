#ifndef SETTINGSMODEL_H
#define SETTINGSMODEL_H

#include "enums.h"

#include <QObject>

class SettingsModel
{
public:
    SettingsModel();

    void save_tabs(QMap<WidgetSide, QList<QString>> paths);
    QMap<WidgetSide, QList<QString>> read_tabs();
};

#endif // SETTINGSMODEL_H
