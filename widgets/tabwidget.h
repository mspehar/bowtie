#ifndef TABWIDGET_H
#define TABWIDGET_H

#include "../models/enums.h"
#include "../models/modelmanager.h"

#include <QTabWidget>

class TabWidget : public QTabWidget
{
    Q_OBJECT

public:
    TabWidget(ModelManager *manager, WidgetSide side, QWidget *parent);

    void create_tab(QString path);
    void take_focus();
    QString current_path();
    QList<QString> paths();

private:
    ModelManager* m_manager;
    WidgetSide m_side;

    void keyPressEvent(QKeyEvent* event);

private slots:
    void close_tab(int index);

public slots:
    void change_tab_text(QString text);
    void activate_drive_combobox();

signals:
    void request_switch_side();
    void request_change_tab_text(QString text);
    void request_copy_dialog(QList<QString> item_list);
    void request_move_dialog(QList<QString> item_list);
};

#endif // TABWIDGET_H
