#include "splitter.h"

#include "tabwidget.h"

#include "../models/enums.h"
#include "../dialogs/movecopydialog.h"
#include "../models/modelmanager.h"

#include <QMainWindow>
#include <QKeyEvent>

Splitter::Splitter(ModelManager *manager, QWidget *parent)
    : QSplitter(parent)
    , m_manager(manager)
{
    m_left_tab_widget = new TabWidget(m_manager, WidgetSide::LEFT, this);
    m_right_tab_widget = new TabWidget(m_manager, WidgetSide::RIGHT, this);

    addWidget(m_left_tab_widget);
    addWidget(m_right_tab_widget);

    connect(m_left_tab_widget, &TabWidget::request_switch_side, this, &Splitter::switch_side);
    connect(m_right_tab_widget, &TabWidget::request_switch_side, this, &Splitter::switch_side);

    connect(m_left_tab_widget, &TabWidget::request_copy_dialog, this, &Splitter::show_copy_dialog);
    connect(m_right_tab_widget, &TabWidget::request_copy_dialog, this, &Splitter::show_copy_dialog);

    connect(m_left_tab_widget, &TabWidget::request_move_dialog, this, &Splitter::show_move_dialog);
    connect(m_right_tab_widget, &TabWidget::request_move_dialog, this, &Splitter::show_move_dialog);
}

void Splitter::switch_side()
{
    QObject *s = sender();

    if (s == m_left_tab_widget) {
        m_right_tab_widget->take_focus();
    } else {
        m_left_tab_widget->take_focus();
    }
}


void Splitter::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Tab && event->modifiers() == Qt::NoModifier) {
        QWidget::keyPressEvent(event);
    }
    else if (event->keyCombination() == QKeyCombination(Qt::AltModifier, Qt::Key_F1) ) {
        m_left_tab_widget->activate_drive_combobox();
    }
    else if (event->keyCombination() == QKeyCombination(Qt::AltModifier, Qt::Key_F2) ) {
        m_right_tab_widget->activate_drive_combobox();
    }
    else {
        QWidget::keyPressEvent(event);
    }
}

QString Splitter::opposite_path(WidgetSide side) const
{
    qDebug() << side;

    if (side == WidgetSide::LEFT) {
        return m_right_tab_widget->current_path();
    } else {
        return m_left_tab_widget->current_path();
    }
}

void Splitter::show_copy_dialog(QList<QString> item_list)
{
    QObject *s = sender();

    QString source_dir;
    QString destination_dir;

    if (s == m_left_tab_widget) {
        source_dir = m_left_tab_widget->current_path();
        destination_dir = m_right_tab_widget->current_path();
    } else {
        source_dir = m_right_tab_widget->current_path();
        destination_dir = m_left_tab_widget->current_path();
    }

    MoveCopyDialog d(m_manager, item_list, source_dir, destination_dir, FileSystemOperationType::COPY, this);
    d.exec();
}


void Splitter::show_move_dialog(QList<QString> item_list)
{
    QObject *s = sender();

    QString source_dir;
    QString destination_dir;

    if (s == m_left_tab_widget) {
        source_dir = m_left_tab_widget->current_path();
        destination_dir = m_right_tab_widget->current_path();
    } else {
        source_dir = m_right_tab_widget->current_path();
        destination_dir = m_left_tab_widget->current_path();
    }

    MoveCopyDialog d(m_manager, item_list, source_dir, destination_dir, FileSystemOperationType::MOVE, this);
    d.exec();
}

QMap<WidgetSide, QList<QString>> Splitter::paths()
{
    QMap<WidgetSide, QList<QString>> _map;

    _map[WidgetSide::LEFT] = m_left_tab_widget->paths();
    _map[WidgetSide::RIGHT] = m_right_tab_widget->paths();

    return _map;
}
