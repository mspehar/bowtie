#ifndef SPLITTER_H
#define SPLITTER_H

#include "tabwidget.h"

#include "../models/enums.h"
#include "../models/modelmanager.h"

#include <QMainWindow>
#include <QSplitter>

class Splitter : public QSplitter
{
    Q_OBJECT

public:
    Splitter(ModelManager *manager, QWidget *parent = 0);

    QString opposite_path(WidgetSide side) const;
    QMap<WidgetSide, QList<QString>> paths();

private:
    ModelManager *m_manager;

    TabWidget *m_left_tab_widget;
    TabWidget *m_right_tab_widget;

    void keyPressEvent(QKeyEvent* event);

public slots:
    void switch_side();
    void show_copy_dialog(QList<QString> item_list);
    void show_move_dialog(QList<QString> item_list);

};

#endif // SPLITTER_H
