#include "mainwindow.h"

#include "../models/modelmanager.h"
#include "splitter.h"

#include <QAction>
#include <QMenu>
#include <QMenuBar>

MainWindow::MainWindow(ModelManager *manager, QWidget *parent)
    : QMainWindow(parent)
    , m_manager(manager)
{
    m_splitter = new Splitter(m_manager, this);

    setup_ui();
}

MainWindow::~MainWindow()
{
    m_manager->m_settings_model.save_tabs(m_splitter->paths());

    delete m_manager;
}

void MainWindow::setup_ui()
{
    setWindowTitle("Bowtie");

    resize(1200, 600);

    setCentralWidget(m_splitter);
}
