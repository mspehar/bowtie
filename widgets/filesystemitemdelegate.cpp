#include "filesystemitemdelegate.h"

#include "../models/enums.h"

#include <QColor>
#include <QPalette>

#include <QStyleOptionViewItem>

FileSystemItemDelegate::FileSystemItemDelegate() : QStyledItemDelegate()
{

}

void FileSystemItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem opt(option);
    initStyleOption(&opt, index);

    if(index.data(DataRoles::SELECTED).value<bool>()){
        //opt.palette.setColor(QPalette::Normal, QPalette::ColorRole, Qt::red);
        opt.palette.setColor(QPalette::Text, QColor("red"));
        opt.palette.setColor(QPalette::HighlightedText, QColor("red"));
    }

    QStyledItemDelegate::paint(painter, opt, index);
}


void FileSystemItemDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);

    if(index.column() == 0){
        option->displayAlignment = Qt::AlignVCenter | Qt::AlignLeft;
    }
    else if(index.column() == 1){
        option->displayAlignment = Qt::AlignVCenter | Qt::AlignRight;
    }
    else if(index.column() == 2){
        option->displayAlignment = Qt::AlignVCenter | Qt::AlignRight;
    }
}
