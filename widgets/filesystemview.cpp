#include "filesystemview.h"

#include "../models/filesystemmodel.h"
#include "../dialogs/createdirectorydialog.h"
#include "../dialogs/deletedialog.h"

#include <QEvent>
#include <QFileInfo>
#include <QFileSystemModel>
#include <QHeaderView>
#include <QKeyEvent>
#include <QTreeView>

FileSystemView::FileSystemView(FileSystemModel *model, QWidget *parent)
    : QTreeView(parent)
    , m_model(model)
{
    setItemDelegate(&m_delegate);

    //setGridStyle(Qt::NoPen);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setSelectionBehavior (QAbstractItemView::SelectRows);
    setWordWrap(false);

    setRootIsDecorated(false);
    setItemsExpandable(false);
    setExpandsOnDoubleClick(false);
    setSortingEnabled(true);

    QFont font("Cascadia Mono");
    font.setPointSize(10);

    setFont(font);

    resize_columns_to_standard();

    connect(m_model, &FileSystemModel::path_changed, this, &FileSystemView::path_changed);
}

void FileSystemView::mouseDoubleClickEvent(QMouseEvent* event)
{
    Q_UNUSED(event);

    if (currentIndex().isValid()) {
        QFileInfo _path = m_model->file_info(currentIndex());

        if (_path.isDir()) {
            m_stack.push(currentIndex());
            m_model->set_path(_path.absoluteFilePath());
        }
    }
}

void FileSystemView::resize_columns_to_standard() {
    for (int i=0; i < header()->count(); i++) {
        if (i==0) {
            header()->setSectionResizeMode(i, QHeaderView::Stretch);
        } else {
            header()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
        }
    }

    /*
    for (int i=0; i<horizontalHeader()->count(); i++) {
        if (i==0) {
            horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
        } else {
            horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
        }
    }
    */
}

void FileSystemView::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
        if (currentIndex().isValid()) {
            QFileInfo _path = m_model->file_info(currentIndex());

            if (_path.isDir()) {
                m_stack.push(currentIndex());
                m_model->set_path(_path.absoluteFilePath());
            }
        }
    }
    else if (event->key() == Qt::Key_Backspace) {
        QFileInfo _path(m_model->path());
        QDir _dir = _path.dir();

        m_model->set_path(_dir.absolutePath());
    }
    else if (event->key() == Qt::Key_Tab && event->modifiers() == Qt::NoModifier) {
        QWidget::keyPressEvent(event);
    }
    else if (event->key() == Qt::Key_F3) {
    }
    else if (event->key() == Qt::Key_F4) {
    }
    else if (event->key() == Qt::Key_F5) {
        if (m_model->marked_count() > 0) {
            emit request_copy_dialog(m_model->marked_list());
        } else if (currentIndex().isValid() && m_model->file_info(currentIndex()).fileName() != "..") {
            QList<QString> marked_list;
            marked_list.append(m_model->file_info(currentIndex()).absoluteFilePath());

            emit request_copy_dialog(marked_list);
        }
    }
    else if (event->key() == Qt::Key_F6) {
        if (m_model->marked_count() > 0) {
            emit request_move_dialog(m_model->marked_list());
        } else if (currentIndex().isValid() && m_model->file_info(currentIndex()).fileName() != "..") {
            QList<QString> marked_list;
            marked_list.append(m_model->file_info(currentIndex()).absoluteFilePath());

            emit request_move_dialog(marked_list);
        }
    }
    else if (event->key() == Qt::Key_F7) {
        CreateDirectoryDialog dialog(m_model, this);
        dialog.exec();
    }
    else if (event->key() == Qt::Key_F8) {
        if (m_model->marked_count() > 0) {
            DeleteDialog dialog(m_model->marked_list(), this);
            dialog.exec();
        } else if (currentIndex().isValid() && m_model->file_info(currentIndex()).fileName() != "..") {
            QList<QString> marked_list;
            marked_list.append(m_model->file_info(currentIndex()).absoluteFilePath());

            DeleteDialog dialog(marked_list, this);
            dialog.exec();
        }
    }
    else if (event->keyCombination() == QKeyCombination(Qt::ControlModifier, Qt::Key_R) ) {
        m_model->populate();
    }
    else if (event->key() == Qt::Key_Asterisk ) {
    }
    else if (event->key() == Qt::Key_Space ) {
        if (currentIndex().isValid() && m_model->file_info(currentIndex()).fileName() != "..") {
            m_model->flip_marked(currentIndex());
        }
    }
    else if (event->key() == Qt::Key_Insert) {
        if (currentIndex().isValid() && m_model->file_info(currentIndex()).fileName() != "..") {
            m_model->flip_marked(currentIndex());

            int next_index = std::min(model()->rowCount()-1, currentIndex().row()+1);
            setCurrentIndex(currentIndex().siblingAtRow(next_index));
        }
    }
    else {
        QTreeView::keyPressEvent(event);
    }
}

void FileSystemView::path_changed(QString old_path, QString new_path)
{
    if (old_path.length() > new_path.length()) {
        if (!m_stack.isEmpty()) {
            QModelIndex top = m_stack.top();

            if (top.isValid()) {
                setCurrentIndex(m_stack.pop());
            } else {
                setCurrentIndex(m_model->index(0, 0));
            }
        } else {
            setCurrentIndex(m_model->index(0, 0));
        }
    } else {
        setCurrentIndex(m_model->index(0, 0));
    }
}

void FileSystemView::drive_index_changed(int index)
{
    m_stack.clear();
}
