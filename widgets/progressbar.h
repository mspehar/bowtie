#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QProgressBar>
#include <QWidget>

class ProgressBar : public QProgressBar
{
    Q_OBJECT

public:
    ProgressBar(QString text, QWidget *parent = 0);

    QString text() const;

private:
    QString m_text;
};

#endif // PROGRESSBAR_H
