#ifndef FILESYSTEMVIEW_H
#define FILESYSTEMVIEW_H

#include "filesystemitemdelegate.h"
#include "../models/filesystemmodel.h"

#include <QModelIndex>
#include <QStack>
#include <QTreeView>
#include <QWidget>

class FileSystemView : public QTreeView
{
    Q_OBJECT

public:
    FileSystemView(FileSystemModel *model, QWidget *parent = 0);

private:
    FileSystemItemDelegate m_delegate;
    FileSystemModel *m_model;
    QStack<QModelIndex> m_stack;

    void resize_columns_to_standard();

    void mouseDoubleClickEvent(QMouseEvent* event);
    void keyPressEvent(QKeyEvent* event);

public slots:
    void path_changed(QString old_path, QString new_path);
    void drive_index_changed(int index);

signals:
    void request_copy_dialog(QList<QString> item_list);
    void request_move_dialog(QList<QString> item_list);
};

#endif // FILESYSTEMVIEW_H
