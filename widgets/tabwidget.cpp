#include "tabwidget.h"

#include "tab.h"
#include "../models/modelmanager.h"

#include <QKeyEvent>
#include <QMap>
#include <QStorageInfo>
#include <QWidget>

TabWidget::TabWidget(ModelManager *manager, WidgetSide side, QWidget *parent)
    : QTabWidget(parent)
    , m_manager(manager)
    , m_side(side)
{
    setTabsClosable(true);

    QMap<WidgetSide, QList<QString>> map = m_manager->m_settings_model.read_tabs();
    QList<QString> tabs = map[m_side];

    if (tabs.length() == 0) {
        create_tab(QDir::homePath());
    } else {
        foreach(QString _path, tabs) {
            create_tab(_path);
        }
    }

    connect(this, &TabWidget::tabCloseRequested, this, &TabWidget::close_tab);
}

void TabWidget::create_tab(QString path)
{
    FileSystemModel *model = new FileSystemModel(this);
    Tab* new_tab = new Tab(m_manager, m_side, model, path, this);

    QDir d(path);
    QString dir_name;

    if (d.isRoot()) {
        dir_name = QStorageInfo(path).rootPath();
    } else {
        dir_name = QDir(path).dirName();
    }

    addTab(new_tab, dir_name);

    /* do something with the signal*/
    connect(new_tab, &Tab::request_change_tab_text, this, &TabWidget::change_tab_text);

    /* propagate signal up the widget tree */
    connect(new_tab, &Tab::request_switch_side, this, &TabWidget::request_switch_side);
    connect(new_tab, &Tab::request_copy_dialog, this, &TabWidget::request_copy_dialog);
    connect(new_tab, &Tab::request_move_dialog, this, &TabWidget::request_move_dialog);
}

void TabWidget::close_tab(int index)
{
    if (count() > 1) {
        QWidget* tab_to_delete = widget(index);
        removeTab(index);
        tab_to_delete->deleteLater();
    }
}

void TabWidget::keyPressEvent(QKeyEvent* event)
{
    if (event->keyCombination() == QKeyCombination(Qt::ControlModifier, Qt::Key_T)) {
        create_tab(current_path());
        setCurrentIndex(count()-1);
    }
    else if (event->key() == Qt::Key_Backtab) {
        if (currentIndex() == 0) {
            setCurrentIndex(count()-1);
        } else {
            setCurrentIndex(currentIndex()-1);
        }
    }
    else if (event->keyCombination() == QKeyCombination(Qt::ControlModifier, Qt::Key_Tab)) {
        if (currentIndex() == count()-1) {
            setCurrentIndex(0);
        } else {
            setCurrentIndex(currentIndex()+1);
        }
    }
    if (event->keyCombination() == QKeyCombination(Qt::ControlModifier, Qt::Key_W)) {
        close_tab(currentIndex());
    }
    else {
        QWidget::keyPressEvent(event);
    }
}

void TabWidget::take_focus()
{
    Tab* current_tab = static_cast<Tab*>(currentWidget());
    current_tab->set_focus();
}

void TabWidget::change_tab_text(QString text)
{
    setTabText(currentIndex(), text);
}

QString TabWidget::current_path()
{
    Tab* current_tab = static_cast<Tab*>(currentWidget());
    return current_tab->path();
}

void TabWidget::activate_drive_combobox()
{
    Tab* current_tab = static_cast<Tab*>(currentWidget());
    current_tab->activate_drive_combobox();
}

QList<QString> TabWidget::paths()
{
    QList<QString> _paths;

    for (int i=0; i<count(); i++) {
        Tab* tab = static_cast<Tab*>(widget(i));
        _paths.append(tab->path());
    }

    return _paths;
}
