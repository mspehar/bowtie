#ifndef FILESYSTEMITEMDELEGATE_H
#define FILESYSTEMITEMDELEGATE_H

#include <QStyledItemDelegate>

class FileSystemItemDelegate : public QStyledItemDelegate
{
public:
    FileSystemItemDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
    QVector<int> m_rows;

protected:
    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const override;
};

#endif // FILESYSTEMITEMDELEGATE_H
