#ifndef TAB_H
#define TAB_H

#include "filesystemview.h"

#include "../models/enums.h"
#include "../models/filesystemmodel.h"
#include "../models/modelmanager.h"


#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>

class Tab : public QWidget
{
    Q_OBJECT

public:
    Tab(ModelManager *manager, WidgetSide side, FileSystemModel *model, QString path, QWidget *parent = 0);

    void set_focus();
    QString path();
    void activate_drive_combobox();
    void set_path(QString new_path);

private:
    ModelManager *m_manager;
    WidgetSide m_side;
    FileSystemModel *m_model;
    QTimer* m_timer;
    QStringListModel *m_drive_model;

    QLineEdit* m_line_edit;
    QComboBox* m_drive_view;
    QPushButton* m_button;
    FileSystemView* m_view;
    QProgressBar* m_progress_bar;

    void setup_ui();

    void keyPressEvent(QKeyEvent* event);
    bool focusNextPrevChild(bool next);

private slots:
    void refresh_storage_usage();
    void refresh_drives();

    void path_changed(QString old_path, QString new_path);
    void drive_index_changed(int index);

signals:
    void request_switch_side();
    void request_change_tab_text(QString text);
    void request_copy_dialog(QList<QString> item_list);
    void request_move_dialog(QList<QString> item_list);
};

#endif // TAB_H
