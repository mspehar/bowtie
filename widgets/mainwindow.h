#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../models/modelmanager.h"

#include "splitter.h"

#include <QMainWindow>
#include <QMenuBar>
#include <QSplitter>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(ModelManager *manager, QWidget *parent = nullptr);
    ~MainWindow();

private:
    Splitter *m_splitter;

    ModelManager *m_manager;

    void setup_ui();
};
#endif // MAINWINDOW_H
