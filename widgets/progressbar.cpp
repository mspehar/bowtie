#include "progressbar.h"

#include <QWidget>

ProgressBar::ProgressBar(QString text, QWidget *parent)
    : QProgressBar(parent)
    , m_text(text)
{

}

QString ProgressBar::text() const
{
    return m_text;
}
