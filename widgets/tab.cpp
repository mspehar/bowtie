#include "tab.h"

#include "../models/filesystemmodel.h"

#include <QComboBox>
#include <QEvent>
#include <QKeyEvent>
#include <QProgressBar>
#include <QStorageInfo>
#include <QTimer>

Tab::Tab(ModelManager *manager, WidgetSide side, FileSystemModel *model, QString path, QWidget *parent)
    : QWidget(parent)
    , m_manager(manager)
    , m_side(side)
    , m_model(model)
{
    m_view = new FileSystemView(m_model, this);
    m_view->setModel(m_model);

    m_line_edit = new QLineEdit(this);
    m_line_edit->setText(m_model->path());

    m_drive_model = new QStringListModel(this);
    m_drive_view = new QComboBox(this);
    m_drive_view->setModel(m_drive_model);

    m_progress_bar = new QProgressBar(this);
    m_progress_bar->setTextVisible(false);
    m_progress_bar->setStyleSheet("QProgressBar { border: 0px solid grey; min-height: 2px; max-height:2px; padding: 0px; margin: 0px; border-radius: 0px; text-align: center; } QProgressBar::chunk {background-color: #3add36; width: 1px; min-height: 2px; max-height: 2px;}");

    m_timer = new QTimer(this);
    m_timer->setInterval(1000);
    m_timer->start();

    refresh_storage_usage();

    connect(m_view, &FileSystemView::request_copy_dialog, this, &Tab::request_copy_dialog);
    connect(m_view, &FileSystemView::request_move_dialog, this, &Tab::request_move_dialog);
    connect(m_timer, &QTimer::timeout, this, &Tab::refresh_storage_usage);
    connect(m_model, &FileSystemModel::path_changed, this, &Tab::path_changed);

    set_path(path);

    setup_ui();
}

void Tab::setup_ui()
{
    setLayout(new QVBoxLayout(this));

    QWidget* h_top = new QWidget(this);
    h_top->setLayout(new QHBoxLayout(h_top));
    h_top->layout()->addWidget(m_drive_view);
    h_top->layout()->addWidget(m_line_edit);
    h_top->layout()->setContentsMargins(QMargins(0, 0, 0, 0));
    m_line_edit->setEnabled(false);

    layout()->addWidget(h_top);
    layout()->addWidget(m_progress_bar);
    layout()->addWidget(m_view);

    refresh_drives();
}

void Tab::refresh_storage_usage()
{
    if (m_model->storage_usage() > 90.0) {
        m_progress_bar->setStyleSheet("QProgressBar { border: 0px solid grey; min-height: 2px; max-height:2px; padding: 0px; margin: 0px; border-radius: 0px; text-align: center; } QProgressBar::chunk {background-color: #ff0000; width: 1px; min-height: 2px; max-height: 2px;}");
    } else {
        m_progress_bar->setStyleSheet("QProgressBar { border: 0px solid grey; min-height: 2px; max-height:2px; padding: 0px; margin: 0px; border-radius: 0px; text-align: center; } QProgressBar::chunk {background-color: #3add36; width: 1px; min-height: 2px; max-height: 2px;}");
    }

    m_progress_bar->setValue(m_model->storage_usage());
}

void Tab::path_changed(QString old_path, QString new_path)
{
    m_line_edit->setText(new_path);

    QDir d(new_path);
    QStorageInfo si(new_path);
    if (d.isRoot()) {
        emit request_change_tab_text(si.rootPath());
    } else {
        emit request_change_tab_text(d.dirName());
    }
}

void Tab::set_path(QString new_path)
{
    m_model->set_path(new_path);

    QDir d(new_path);
    QStorageInfo si(new_path);
    if (d.isRoot()) {
        emit request_change_tab_text(si.rootPath());
    } else {
        emit request_change_tab_text(d.dirName());
    }
}


void Tab::drive_index_changed(int index)
{
    if (index >= 0) {
        QString drive = m_drive_view->itemText(index);
        m_model->set_path(drive);
    }

    refresh_storage_usage();
}

void Tab::refresh_drives()
{
    disconnect(m_drive_view, &QComboBox::currentIndexChanged, m_view, &FileSystemView::drive_index_changed);
    disconnect(m_drive_view, &QComboBox::currentIndexChanged, this, &Tab::drive_index_changed);

    QStringList drive_list;

    foreach(QStorageInfo si, QStorageInfo::mountedVolumes()) {
        drive_list.append(si.rootPath());
    }

    m_drive_model->setStringList(drive_list);

    for (int i=0; i < drive_list.count(); i++){
        QString root_path = drive_list[i];

        if (root_path == QStorageInfo(m_model->path()).rootPath()) {
            m_drive_view->setCurrentIndex(i);
            break;
        }
    }

    connect(m_drive_view, &QComboBox::currentIndexChanged, this, &Tab::drive_index_changed);
    connect(m_drive_view, &QComboBox::currentIndexChanged, m_view, &FileSystemView::drive_index_changed);
}

void Tab::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Tab && event->modifiers() == Qt::NoModifier) {
        emit request_switch_side();
    }
    else {
        QWidget::keyPressEvent(event);
    }
}

void Tab::set_focus()
{
    m_view->setFocus();
}

bool Tab::focusNextPrevChild(bool next)
{
    return false;
}

QString Tab::path()
{
    return m_model->path();
    qDebug() << m_model->path();
}

void Tab::activate_drive_combobox()
{
    refresh_drives();
    m_drive_view->showPopup();
    set_focus();
}
